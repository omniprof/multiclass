package com.cejv416.multiclass;

/**
 * Example of using multiple classes
 */
public class MultipleClassApp {

    private final Input in;
    private final Process pro;
    private final Output out;
    private final ReversingStringData rsd;

    /**
     * Default Constructor Initializes all instance objects
     */
    public MultipleClassApp() {
        // Instantiate the objects that this class will use
        rsd = new ReversingStringData();
        in = new Input();
        pro = new Process();
        out = new Output();
    }

    /**
     * Send message to the objects to perform the task of reversing a string
     */
    public void perform() {
        // Get input
        in.requestStringFromUser(rsd);
        // Process the data
        pro.reverse(rsd);
        // Display the result
        out.display(rsd);
    }

    /**
     * Where it begins
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new MultipleClassApp().perform();
    }
}
